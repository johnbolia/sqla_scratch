"""Common operations and testing for VSdataweb"""

from __future__ import annotations
from typing import TYPE_CHECKING, Dict, List, Tuple, Union, Optional, Iterable
from datetime import datetime, timezone
import uuid
from dataclasses import dataclass, asdict, fields

from relationaldb.db_schema import Base, User, Rider, UserRiderMap, RunTable, GroupDescription, GroupRun,\
    MetadataTable, CalType
import sqlalchemy

if TYPE_CHECKING:
    from sqlalchemy.orm import query
    from sqlalchemy.orm.session import Session

DATE_FMT = "%Y_%m%d_%H%M%S"
NAME_FMT_FILE = f"data_{DATE_FMT}.zip"

uuid_or_str = Union[uuid.UUID, str]

#################
# UserSession
#################
class UserSession(object):
    """Retains session and email
    self.rider will be the first rider added to the user"""

    def __init__(self, session: Session, email: str):
        self.session = session
        self.email = email
        self.user = get_user(self.session, self.email)
        self.rider_list = get_all_riders(self.session, self.user)

    @classmethod
    def create_user_only(cls, session: Session, email: str, username: Optional[str] = None) -> UserSession:
        """Will fail on improper email address or existing email address"""
        create_user(session, email, username)
        return cls(session, email)

    @classmethod
    def create_user_and_rider(cls, session: Session, email: str, rider_name: str,
                              username: Optional[str] = None) -> UserSession:
        """Will fail on improper email address or existing email address"""
        user = create_user(session, email, username)
        create_rider(session, user, rider_name)
        return cls(session, email)

    @property
    def rider(self):
        """Returns the first rider in rider list, will fail if no riders"""
        return self.rider_list[0]

    def add_existing_rider(self, rider_email: str, rider_name: str):
        """Adds existing rider to this user"""
        rider = get_rider_from_name(self.session, rider_email, rider_name)
        user_rider = UserRiderMap(UserId=self.user.UserId, RiderId=rider.RiderId)
        self.session.add(user_rider)
        self.session.commit()
        self.rider_list = get_all_riders(self.session, self.user)

    def put_run(self, run_filename: str, rider_name: Optional[str] = None) -> RunTable:
        """Inserts run into database"""
        run_datetime = self._filename_to_datetime(run_filename)
        rider = self._get_rider_from_name(rider_name)
        run_table = RunTable(RiderId=rider.RiderId, RunTs=run_datetime)
        self.session.add(run_table)
        self.session.commit()
        init_run_metadata = gen_dummy_metadata()  # Todo: UPDATE THIS!!!!!!!!!
        metadata_obj = MetadataEntry.from_dict(run_table.RunId, init_run_metadata, CalTypeConfig.init())
        self.put_metadata_table(metadata_obj)
        return run_table

    def get_run_id_from_filename(self, run_filename: str) -> uuid.UUID:
        """run_id from filename"""
        run_datetime = self._filename_to_datetime(run_filename)
        run_table: RunTable = self.session.query(RunTable).\
            filter(RunTable.RunTs == run_datetime).first()
        return run_table.RunId

    def _get_run_filename_from_run_table_obj(self, run_table: RunTable) -> str:
        """filename from run_table_obj"""
        dt: datetime = run_table.RunTs
        return dt.strftime(NAME_FMT_FILE)

    def _filename_to_datetime(self, run_filename: str) -> datetime:
        """Converts filename to datetime"""
        return datetime.strptime(run_filename, NAME_FMT_FILE)

    def put_group(self, description: str, group_tag: str) -> str:
        """Creates new group"""
        group_description = GroupDescription(UserId=self.user.UserId, GroupTag=group_tag, Description=description)
        self.session.add(group_description)
        self.session.commit()
        return group_description.GroupId

    def edit_group_description_and_tag(self, group_id: uuid_or_str, description: str, group_tag: str):
        """Edits group description"""
        group_description: GroupDescription = self.session.query(GroupDescription).\
            filter(GroupDescription.GroupId == uuid_conv(group_id)).first()
        group_description.Description = description
        group_description.GroupTag = group_tag
        self.session.commit()

    def get_group_id_from_description(self, description: str) -> uuid.UUID:
        """Returns GroupId from description"""
        group_description: GroupDescription = self.session.query(GroupDescription).\
            filter(GroupDescription.Description == description).first()
        return group_description.GroupId

    def add_run_to_group(self, group_id: uuid_or_str, run_id: uuid_or_str):
        """Adds run to group, will raise KeyError if check id for group or run fails"""
        group_description: GroupDescription = self.session.query(GroupDescription).\
            filter(GroupDescription.GroupId == uuid_conv(group_id)).first()
        run_table: RunTable = self.session.query(RunTable).\
            filter(RunTable.RunId == uuid_conv(run_id)).first()
        if group_description and run_table:
            group_run = GroupRun(GroupId=uuid_conv(group_id), RunId=uuid_conv(run_id))
            self.session.add(group_run)
            self.session.commit()
        elif group_description is None:
            raise KeyError(f'group_id {group_id} could not be found')
        elif run_table is None:
            raise KeyError(f'run_id {run_id} could not be found')

    def put_metadata_table(self, mde: MetadataEntry):
        """putter"""
        metadata_table = MetadataTable(**asdict(mde))
        self.session.add(metadata_table)
        self.session.commit()

    def get_run_metadata(self, run_filename: str) -> MetadataTable:
        """getter"""
        run_id = self.get_run_id_from_filename(run_filename)
        return self.session.query(MetadataTable).filter(MetadataTable.RunId == run_id).\
            order_by(sqlalchemy.sql.expression.desc('EditTs')).first()

    def edit_run_metadata(self, run_filename: str, values_dict: Dict[str, any]):
        """setter"""
        run_metadata = self.get_run_metadata(run_filename)
        for key, value in values_dict.items():
            setattr(run_metadata, key, value)
        self.session.commit()

    def get_all_rider_runs(self, rider_name: Optional[str] = None) -> List[RunTable]:
        """Gets all runs for rider"""
        rider = self._get_rider_from_name(rider_name)
        return self.session.query(RunTable).filter(RunTable.RiderId == rider.RiderId).all()

    def get_all_user_runs(self) -> List[RunTable]:
        """Retrieves all runs for all riders"""
        all_runs = []
        for rider in self.rider_list:
            all_runs += self.get_all_rider_runs(rider.RiderName)
        return all_runs

    def _get_rider_from_name(self, rider_name: Optional[str] = None) -> Rider:
        """Gets Rider obj from name, if None, default rider"""
        if rider_name is None:
            return self.rider
        for rider_ in self.rider_list:
            if rider_.RiderName == rider_name:
                return rider_
        raise KeyError(f'Rider with RiderName {rider_name} not found')


@dataclass
class MetadataEntry(object):
    """Class containing all attributes for run metadata"""
    RunId: uuid.UUID
    EditTs: datetime
    RunDescription: str
    RiderNameRun: str
    CombinedMass: float
    WhlCircum: float
    PwrMtrSlope: float
    Crr: float
    CpCorr: float
    WhlCircumCalType: int
    PwrMtrSlopeCalType: int
    CrrCalType: int
    CpCorrCalType: int
    DataHashId: uuid.UUID

    @classmethod
    def from_dict(cls, run_id: uuid.UUID, m_dict: Dict, cal_config: CalTypeConfig):
        """Creates class from run_id, metadict and CalTypeConfigOption"""
        name_dict = {'RunDescription': 'description',
                     'RiderNameRun': 'rider',
                     'CombinedMass': 'masstotal_mc',
                     'WhlCircum': 'whlcrc_sc',
                     'PwrMtrSlope': 'riderpwrcorrec_xc',
                     'Crr': 'rollingcrr_xc',
                     'CpCorr': 'aerocpcorrec_xc'}
        input_dict = {'RunId': uuid_conv(run_id),
                      'EditTs': datetime.now(timezone.utc),
                      'DataHashId': uuid.uuid4()}
        for field in fields(cls):
            m_name = name_dict.get(field.name, field.name)
            if m_name in m_dict:
                input_dict[field.name] = eval(field.type)(m_dict[m_name])
                if hasattr(cal_config, field.name):
                    type_name = cal_config.extension(field.name)
                    input_dict[type_name] = getattr(cal_config, field.name).value
        return cls(**input_dict)  # Will fail on missing key


@dataclass
class CalTypeConfig(object):
    """Stores cal types"""
    WhlCircum: CalType
    PwrMtrSlope: CalType
    Crr: CalType
    CpCorr: CalType

    @classmethod
    def init(cls) -> CalTypeConfig:
        return cls(*(CalType(0),) * 4)

    @staticmethod
    def extension(attr: str) -> str:
        return attr + 'CalType'


##################
# Common Functions
##################
def create_user(session: Session, email: str, username: Optional[str] = None) -> User:
    """Adds user and rider, do not use if user does not create new rider"""
    loc_at = email.find('@')
    if loc_at == -1:
        raise ValueError('Invalid email address')
    try:
        if username is None:
            username = email[:loc_at]
        user = User(EmailAddress=email, UserName=username)
        session.add(user)
        session.commit()
        return user
    except sqlalchemy.exc.IntegrityError as e:
        raise ValueError(f'Could not add user {email}, \n{e}')


def create_rider(session: Session, user: User, rider_name: str) -> Rider:
    """Adds rider and maps to user"""
    rider = Rider(RiderName=rider_name, UserId=user.UserId)
    session.add(rider)
    session.commit()
    user_rider = UserRiderMap(UserId=user.UserId, RiderId=rider.RiderId)
    session.add(user_rider)
    session.commit()
    return rider


def get_user(session: Session, email: str) -> User:
    """Gets user object from email address"""
    return session.query(User).filter_by(EmailAddress=email).first()


def get_all_riders(session: Session, user: User) -> List[Rider]:
    # return session.query(UserRiderMap).filter_by(UserId=user.UserId)
    return session.query(Rider).join(UserRiderMap).join(User).filter(User.UserId == user.UserId).all()


def get_rider_from_name(session: Session, email: str, rider_name: str) -> Rider:
    rider = session.query(Rider).\
                join(UserRiderMap).\
                join(User).\
                filter(User.EmailAddress == email).\
                filter(UserRiderMap.UserId == User.UserId).\
                filter(Rider.RiderId == UserRiderMap.RiderId).\
                filter(Rider.RiderName == rider_name).first()
    # print(q, type(q), rider_name)
    return rider


################
# misc functions
################
def flatten_dict(some_dict) -> Dict[any, any]:
    """Converts 2 layer dict into flat dict"""
    new_dict = {}
    for section, section_dict in some_dict.items():
        new_dict.update({key: value for key, value in section_dict.items()})
    return new_dict


def uuid_conv(uuid_candidate: uuid_or_str) -> uuid.UUID:
        if isinstance(uuid_candidate, uuid.UUID):
            return uuid_candidate
        return uuid.UUID(uuid_candidate)


################
# Test Functions
################
def test_env_session() -> Session:
    from sqlalchemy import create_engine
    from sqlalchemy.orm import sessionmaker
    engine = create_engine('sqlite:///:memory:')
    # engine = create_engine('sqlite:///:memory:', echo=True)
    session_class = sessionmaker(bind=engine)
    Base.metadata.create_all(engine)
    return session_class()


def test_db():
    session = test_env_session()
    user_obj = UserSession.create_user_and_rider(session, 'mrnoname@gmail.com', 'Julius', 'Blondie')
    user_obj2 = UserSession.create_user_and_rider(session, 'mrbigshot@gmail.com', 'Irving')
    user_obj3 = UserSession.create_user_and_rider(session, 'mrBrown@gmail.com', 'Berlin', 'ThePoet')
    get_rider_from_name(user_obj2.session, user_obj2.user.EmailAddress, user_obj2.rider.RiderName)
    run0 = user_obj.put_run('data_2020_1009_135141.zip')
    run1 = user_obj3.put_run('data_2020_1009_135141.zip')
    run2 = user_obj3.put_run('data_2020_1009_135144.zip')
    user_obj3.add_existing_rider('mrnoname@gmail.com', 'Julius')
    group0 = user_obj3.put_group('First Group', 'Taggy McTaggart')
    user_obj3.add_run_to_group(group0, run0.RunId)
    user_obj3.add_run_to_group(group0, run1.RunId)
    run0_metadata = MetadataEntry.from_dict(run0.RunId, gen_dummy_metadata(), CalTypeConfig.init())
    user_obj3.put_metadata_table(run0_metadata)
    user_obj3.edit_run_metadata('data_2020_1009_135141.zip', {'RunDescription': 'Destroy!!!'})
    print(user_obj3.get_run_metadata('data_2020_1009_135141.zip'))


def gen_dummy_metadata():
    from relationaldb.dummy_data import DUMMY_DATA
    return DUMMY_DATA


