"""Schema for VSdataweb
use db_schema_functions for common operations and testing"""

from __future__ import annotations
from typing import TYPE_CHECKING, Dict, List, Tuple, Union, Optional
from types import SimpleNamespace
from enum import Enum

from sqlalchemy import Column, Integer, String, Float, DateTime, JSON, TypeDecorator, ForeignKey, UniqueConstraint
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
import uuid
if TYPE_CHECKING:
    pass

Base = declarative_base()


################
# Helper Classes
################
class GUID(TypeDecorator):
    impl = String(32)

    def process_bind_param(self, value, dialect):
        if value is None:
            return value
        elif dialect.name == 'postgresql':
            return str(value)
        else:
            if not isinstance(value, uuid.UUID):
                return "%.32x" % uuid.UUID(value).int
            else:
                # hexstring
                return "%.32x" % value.int

    def process_result_value(self, value, dialect):
        if value is None:
            return value
        else:
            if not isinstance(value, uuid.UUID):
                value = uuid.UUID(value)
            return value


class ReprMixin(object):
    """Mixin class to create universal __repr__ function"""
    __table__ = SimpleNamespace(columns=[])
    __tablename__ = None

    def __repr__(self):
        columns = [f'{col.key}={getattr(self, col.key)}' for col in self.__table__.columns]
        return f'<{self.__tablename__}({",".join(columns)})>'

    def to_dict(self) -> Dict[str, any]:
        """Generates a list of all table columns"""
        return {col.key: getattr(self, col.key) for col in self.__table__.columns}


##############
# Table Schema
##############
class User(Base, ReprMixin):
    """Maps UserId to EmailAddress, username is optional"""
    __tablename__ = 'User'

    UserId = Column(GUID(), primary_key=True, default=uuid.uuid4)
    EmailAddress = Column(String(60), unique=True)
    UserName = Column(String(64))


class Rider(Base, ReprMixin):
    """Maps RiderId to RiderName, one to one, UserId is initial UserID upon creation"""
    __tablename__ = 'Rider'

    RiderId = Column(GUID(), primary_key=True, default=uuid.uuid4)
    RiderName = Column(String(64), unique=True)
    UserId = Column(ForeignKey('User.UserId'))
    UniqueConstraint(RiderName, UserId, name='user_rider_uniqueness')


class UserRiderMap(Base, ReprMixin):
    """Maps UserId to RiderId, many to many relation, AliasName is created when User has an existing rider of the
    same name but with differing RiderId"""
    __tablename__ = 'UserRiderMap'

    id = Column(Integer, primary_key=True)
    UserId = Column(ForeignKey('User.UserId'))
    RiderId = Column(ForeignKey('Rider.RiderId'))
    AliasName = Column(String(64), nullable=True)
    UniqueConstraint(UserId, AliasName, name='user_rider_alias_uniqueness')

    user_name = relationship("User", foreign_keys=[UserId])
    rider_name = relationship("Rider", foreign_keys=[RiderId])


class RunTable(Base, ReprMixin):
    """Maps RundId to RiderId, run name stored in S3 is combinations of RiderId and RunTs"""
    __tablename__ = 'RunTable'

    RunId = Column(GUID(), primary_key=True, default=uuid.uuid4)
    RiderId = Column(ForeignKey('Rider.RiderId'))
    RunTs = Column(DateTime())
    UniqueConstraint(RiderId, RunTs, name='rider_datetime_uniqueness')


class GroupDescription(Base, ReprMixin):
    """Creates a group with a unique id, each group can contain multiple runs, see GroupRun for these pairs"""
    __tablename__ = 'GroupDescription'

    GroupId = Column(GUID(), primary_key=True, default=uuid.uuid4)
    UserId = Column(ForeignKey('User.UserId'))
    GroupTag = Column(String(64))
    Description = Column(String(1024))
    UniqueConstraint(UserId, Description, name='group_description_uniqueness')


class GroupRun(Base, ReprMixin):
    """Creates a group with a unique id, each group can contain multiple runs, see GroupRun for these pairs"""
    __tablename__ = 'GroupRun'

    id = Column(Integer, primary_key=True)
    GroupId = Column(ForeignKey('GroupDescription.GroupId'))
    RunId = Column(ForeignKey('RunTable.RunId'))
    UniqueConstraint(GroupId, RunId, name='group_run_uniqueness')


class MetadataTable(Base, ReprMixin):
    """Stores editable metadata"""
    __tablename__ = 'MetadataTable'

    id = Column(Integer, primary_key=True)
    RunId = Column(ForeignKey('RunTable.RunId'))
    EditTs = Column(DateTime())
    RunDescription = Column(String(1024))
    RiderNameRun = Column(String(64))  # This may be different from Rider.RiderName
    CombinedMass = Column(Float)
    WhlCircum = Column(Float)
    PwrMtrSlope = Column(Float)
    Crr = Column(Float)
    CpCorr = Column(Float)
    WhlCircumCalType = Column(Integer)  # These will use CalType enum, but not typed
    PwrMtrSlopeCalType = Column(Integer)
    CrrCalType = Column(Integer)
    CpCorrCalType = Column(Integer)
    DataHashId = Column(GUID, unique=True)  # Initially unique, will become a hash of metadata

    # CalType: This may become a table or an enum, don't see the need to nail it down at this point
    # CalType = {'init': 0, 'manual': 1, 'single': 2, 'group': 3, 'group_opt1': 4}


class CalType(Enum):
    """Enum is to specify calibration type"""
    INIT = 0
    MANUAL = 1
    SINGLE = 2
    GROUP = 3
    GROUP_OPT1 = 4


class RunDataCache(Base, ReprMixin):
    """Stores Cached computed results, currently this is just a JSON entry"""
    # Todo: 1) Setup hash function to match MetadataTable.DataHashId to determine if results need re-computation
    #       2) Setup hash function to describe fields to determine if results need re-computation to be compatible
    #       with current results table
    #       3) Break this table into two levels, simple computation based on energy, and full re-computation
    __tablename__ = 'RunDataCache'

    id = Column(Integer, primary_key=True)
    RunId = Column(ForeignKey('RunTable.RunId'))
    RunData = Column(JSON)
    # DataHashId = Column(ForeignKey('MetadataTable.DataHashId'))  # To be enabled in the future
    # ChannelHashId = Column(GUID)  # To be enabled in the future


