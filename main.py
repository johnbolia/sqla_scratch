"""Test module to setup database for VSdataweb"""
from relationaldb import db_schema_functions


if __name__ == "__main__":
    db_schema_functions.test_db()
